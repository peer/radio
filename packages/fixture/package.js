Package.describe({
  name: 'fixture',
  version: '0.1.0'
});

Package.onUse(function (api) {
  api.versionsFrom('1.4.1');

  // Core dependencies.
  api.use([
    'coffeescript',
    'underscore'
  ]);

  // Internal dependencies.
  api.use([
    'core',
    'jobs'
  ]);

  api.addFiles([
    'server.coffee'
  ], 'server');
});
