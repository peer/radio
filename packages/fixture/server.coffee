Meteor.startup ->
  return if PlaylistTrack.documents.exists()

  tracks = Track.documents.find({}, fields: Track.PUBLISH_FIELDS(), transform: null).fetch()
  tracks = _.shuffle tracks
  for track in tracks
    PlaylistTrack.addTrack track

  new ScheduleJob().enqueue()
