new PublishEndpoint 'Playlist.pendingTracks', ->
  PlaylistTrack.documents.find
    state: PlaylistTrack.STATE.PENDING
    deleted:
      $ne: true
  ,
    sort:
      order: 1

new PublishEndpoint 'Playlist.playingTracks', ->
  PlaylistTrack.documents.find
    state: PlaylistTrack.STATE.PLAYING
    deleted:
      $ne: true
