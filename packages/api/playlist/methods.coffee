Meteor.methods
  'Playlist.setTrackOrder': (trackId, newOrder) ->
    check trackId, Match.DocumentId
    check newOrder, Match.Where (value) =>
      _.isNumber(value) and _.isFinite(value)

    # TODO: Limit only to current DJ.

    PlaylistTrack.documents.update trackId,
      $set:
        order: newOrder
