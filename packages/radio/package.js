Package.describe({
  name: 'radio',
  version: '0.1.0'
});

Package.onUse(function (api) {
  api.versionsFrom('1.4.1');

  // Core dependencies.
  api.use([
    'coffeescript',
    'stylus',
    'ejson'
  ]);

  // 3rd party dependencies.
  api.use([
    'kadira:flow-router@2.12.1',
    'kadira:blaze-layout@2.3.0',
    'peerlibrary:blaze-layout-component@0.2.1',
    'materialize:materialize@0.97.8',
    'momentjs:moment@2.17.1',
    'mizzao:jquery-ui@1.11.4',
    'bojicas:howler2@2.0.0_7',
    'peerlibrary:computed-field@0.6.1',
    'peerlibrary:reactive-field@0.3.0',
    'fourseven:scss@3.9.0'
  ]);

  // Internal dependencies.
  api.use([
    'core',
    'ui-components',
    'fixture',
    'api',
    'jobs'
  ]);

  api.addFiles([
    'startup.coffee'
  ], 'server');

  api.addFiles([
    'flow-router/layout.html',
    'flow-router/layout.coffee',
    'flow-router/layout.styl',
    'flow-router/not-found.html',
    'flow-router/not-found.coffee',
    'flow-router/icons.html',
    'playlist/display.html',
    'playlist/display.coffee',
    'playlist/display.styl',
    'base.scss'
  ], 'client');
});
