Meteor.startup ->
  # When we start let us make sure playlists are correctly scheduled.
  new ScheduleJob().enqueue()
