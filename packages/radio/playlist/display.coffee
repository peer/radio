MEDIA_ERROR_CODE =
  1: 'MEDIA_ERR_ABORTED'
  2: 'MEDIA_ERR_NETWORK'
  3: 'MEDIA_ERR_DECODE'
  4: 'MEDIA_ERR_SRC_NOT_SUPPORTED'

class Playlist.DisplayComponent extends UIComponent
  @register 'Playlist.DisplayComponent'

  onCreated: ->
    super

    @subscribe 'Playlist.playingTracks'
    @subscribe 'Playlist.pendingTracks'

    @trackError = new ReactiveField null

    # In percentage.
    @playingPosition = new ReactiveField 0.0

    # We want to minimize restarting of playback.
    @playingTrack = new ComputedField =>
      PlaylistTrack.documents.findOne
        state: PlaylistTrack.STATE.PLAYING
      ,
        fields:
          playStart: 1
          'track.files': 1
        transform: null
    ,
      EJSON.equals

    @_currentHowl = null
    @_currentHowlHandlers = {}
    @_currentHowlInterval = null
    @_currentHowlSoundId = null

    @autorun (computation) =>
      track = @playingTrack()

      @playHowl track, false

  playHowl: (track, keepError) ->
    @stopCurrentHowl keepError

    return unless track?.track?.files and track.playStart?

    sources = []
    extensions = []
    for format, source of track.track.files
      sources.push source
      extensions.push @mapFormat format

    # TODO: We should optimize the order for formats in these arrays.

    @_currentHowlHandlers = {}
    @_currentHowlHandlers.load = (id) =>
      @onHowlLoad track
    @_currentHowlHandlers.loaderror = (id, msg) =>
      @onHowlLoadError track, msg
    @_currentHowlHandlers.play = (id) =>
      @onHowlPlay track

    @_currentHowl = new Howl
      # We use HTML5 because we do not want to wait for the whole file to load.
      html5: true
      src: sources
      ext: extensions
      volume: 0.0

    for event, handler of @_currentHowlHandlers
      @_currentHowl.on event, handler

    @_currentHowlInterval = Meteor.setInterval =>
      if duration = @_currentHowl.duration()
        @playingPosition (@_currentHowl.seek() / duration) * 100.0
      else
        @playingPosition 0.0
    ,
      250 # ms

    @_currentHowlSoundId = @_currentHowl.play()

  onHowlLoad: (track) ->
    # We remove the error to make sure it is removed if "stopCurrentHowl" was called with "keepError" set.
    @trackError null

    audioNode = @_currentHowl._soundById(@_currentHowlSoundId)._node
    console.error "Track '#{@_currentHowl._src}' is not seekable. Is its server configured to support byte-range requests?" unless audioNode.seekable.length and audioNode.seekable.end(0) isnt 0

    # TODO: This assumes client's clock is accurate. What if it is not? Can we get server's clock somehow?
    initialPosition = Math.max(0.0, (new Date().valueOf() - track.playStart.valueOf()) / 1000.0)

    @_currentHowl.seek initialPosition
    @_currentHowl.fade 0.0, 1.0, 50

  onHowlPlay: (track) ->

  onHowlLoadError: (track, msg) ->
    msg = MEDIA_ERROR_CODE[msg] if "#{msg}" of MEDIA_ERROR_CODE

    console.error "Playback error for track '#{@_currentHowl._src}'", msg

    switch msg
      when 'MEDIA_ERR_SRC_NOT_SUPPORTED'
        @trackError "Error loading the track. Are you signed-in into the file server?"
      when 'MEDIA_ERR_NETWORK'
        @trackError "Loading the track was interrupted. Check your network connectivity."
        @playHowl track, true
      when 'MEDIA_ERR_DECODE'
        @trackError "Error decoding the track."
      when 'No codec support for selected audio sources.'
        @trackError "Your browser is lacking support for decoding the track."
      when 'Decoding audio data failed.'
        @trackError "Error decoding the track."
      when 'No audio support.'
        @trackError "Your browser is lacking support for audio."
      when 'MEDIA_ERR_ABORTED'
        # Ignore, user aborted.
      else
        @trackError msg

  stopCurrentHowl: (keepError) ->
    return unless @_currentHowl
    currentHowl = @_currentHowl
    handlers = @_currentHowlHandlers
    @_currentHowl = null
    @_currentHowlHandlers = {}
    @_currentHowlSoundId = null

    Meteor.clearInterval @_currentHowlInterval
    @_currentHowlInterval = null

    @trackError null unless keepError
    @playingPosition 0.0

    for event, handler of handlers
      currentHowl.off event, handler

    if currentHowl.playing()
      currentHowl.fade 1.0, 0.0, 50
      currentHowl.once 'faded', (id) =>
        currentHowl.stop()
        currentHowl.unload()
    else
      currentHowl.stop()
      currentHowl.unload()

  mapFormat: (format) ->
    # TODO: This is pretty fragile, but it will work for now.
    switch format
      when 'audio/mp3' then 'mp3'
      when 'audio/mpeg' then 'mpeg'
      when 'audio/ogg; codecs="opus"' then 'opus'
      when 'audio/ogg; codecs="vorbis"' then 'ogg'
      when 'audio/wav', 'audio/wav; codecs="1"' then 'wav'
      when 'audio/aac' then 'aac'
      when 'audio/x-m4a', 'audio/m4a', 'audio/aac' then 'm4a'
      when 'audio/x-mp4', 'audio/mp4', 'audio/aac' then 'mp4'
      when 'audio/weba; codecs="vorbis"' then 'weba'
      when 'audio/webm; codecs="vorbis"' then 'webm'
      else throw new Error "Unsupported format '#{format}'."

  onRendered: ->
    super

    if @cssPrefix() in ['webkit', 'moz']
      cursor = "-#{@cssPrefix()}-grabbing"
    else
      cursor = 'grabbing'

    @$('.collection').sortable
      axis: 'y'
      handle: '.drag-handle'
      items: '.collection-item:not(.playing)'
      cursor: cursor
      tolerance: 'pointer'
      stop: (event, ui) =>
        draggedTrackId = UIComponent.getComponentForElement(ui.item.get(0)).data()._id

        if elementBefore = ui.item.prev().get(0)
          beforeTrackOrder = UIComponent.getComponentForElement(elementBefore).data().order ? null
        else
          beforeTrackOrder = null

        if elementAfter = ui.item.next().get(0)
          afterTrackOrder = UIComponent.getComponentForElement(elementAfter).data().order ? null
        else
          afterTrackOrder = null

        if beforeTrackOrder is null and afterTrackOrder is null
          # Not really moving anywhere.
          return
        else if beforeTrackOrder is null
          # Moving to the beginning.
          newOrder = afterTrackOrder - 1
        else if afterTrackOrder is null
          # Moving to the end.
          newOrder = beforeTrackOrder + 1
        else
          # Moving somewhere in between.
          newOrder = (beforeTrackOrder + afterTrackOrder) / 2

        # Do not let jQuery UI move the item but we will change the state in the database and then leave to the Blaze
        # to move the item. This makes sure that UI is in sync with the state in the database (otherwise if the method
        # would fail, for the current user UI would still show item at the new location).
        @$('.collection').sortable 'cancel'

        Meteor.call 'Playlist.setTrackOrder', draggedTrackId, newOrder, (error) =>
          if error
            console.error "Set track order error", error
            alert "Set track order error: #{error.reason or error}"
            return

  pendingTracks: ->
    PlaylistTrack.documents.find
      state:
        $in: [PlaylistTrack.STATE.PENDING, PlaylistTrack.STATE.PLAYING]
    ,
      sort: [
        # We use the fact that "playing" > "pending".
        ['state', 'desc']
        ['order', 'asc']
      ]

class Playlist.TrackComponent extends UIComponent
  @register 'Playlist.TrackComponent'

  lengthMinutes: ->
    seconds = parseInt(@data().track?.length % 60)
    "#{parseInt(@data().track?.length / 60)}:#{if seconds < 10 then '0' else ''}#{seconds}"

  isPlaying: ->
    @data().state is PlaylistTrack.STATE.PLAYING

  playingPosition: ->
    @callAncestorWith 'playingPosition'

FlowRouter.route '/',
  name: 'Playlist.list'
  action: (params, queryParams) ->
    BlazeLayout.render 'MainLayoutComponent',
      main: 'Playlist.DisplayComponent'
