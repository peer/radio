class Album extends share.BaseDocument
  # shivaId
  # name
  # year
  # cover: URL to the image
  # artists: list of
  #   _id
  #   name
  #   image
  # syncId: an ID representing the latest sync run
  # deleted: boolean

  @Meta
    name: 'Album'
    fields: =>
      # Some albums can be without artists, orphans.
      artists: [@ReferenceField Artist, Artist.PUBLISH_FIELDS()]

  @PUBLISH_FIELDS: ->
    name: 1
    year: 1
    cover: 1
    artists: 1

if Meteor.isServer
  Album.Meta.collection._ensureIndex
    shivaId: 1

  Album.Meta.collection._ensureIndex
    syncId: 1
