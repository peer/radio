# TODO: Add statistics information.
#       How many times the track has been played. When was the last time the track was played.

class Track extends share.BaseDocument
  # shivaId
  # bitrate
  # album
  #   _id
  #   name
  #   year
  #   cover
  #   artists
  # artist
  #   _id
  #   name
  #   image
  # length
  # ordinal
  # title
  # files: map of MIME types to URLs
  # syncId: an ID representing the latest sync run
  # deleted: boolean

  @Meta
    name: 'Track'
    fields: =>
      # Some tracks can be without an album, orphans.
      album: @ReferenceField Album, Album.PUBLISH_FIELDS(), false
      # Some tracks can be without an artist, orphans.
      artist: @ReferenceField Artist, Artist.PUBLISH_FIELDS(), false

  @PUBLISH_FIELDS: ->
    album: 1
    artist: 1
    length: 1
    title: 1
    files: 1

  image: ->
    @album?.cover or @artist?.image or '/packages/core/public/track.svg'

if Meteor.isServer
  Track.Meta.collection._ensureIndex
    shivaId: 1

  Track.Meta.collection._ensureIndex
    syncId: 1
