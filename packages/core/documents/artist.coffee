class Artist extends share.BaseDocument
  # shivaId
  # name
  # image: URL of the image
  # syncId: an ID representing the latest sync run
  # deleted: boolean

  @Meta
    name: 'Artist'

  @PUBLISH_FIELDS: ->
    name: 1
    image: 1

if Meteor.isServer
  Artist.Meta.collection._ensureIndex
    shivaId: 1

  Artist.Meta.collection._ensureIndex
    syncId: 1
