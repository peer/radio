# TODO: Add a reference to a playlist document when we will support multiple playlists.

class PlaylistTrack extends share.BaseDocument
  # track
  #   album
  #     _id
  #     name
  #     year
  #     cover
  #     artists
  #   artist
  #     _id
  #     name
  #     image
  #   length
  #   title
  #   files: map of MIME types to URLs
  # order: floating point number representing the future order of the track in the playlist, or null after the track has been played
  # playStart: predicted time when track will play, or time when track has started playing
  # playEnd: predicted time when track will end playing, or time when track has ended playing
  # state

  @Meta
    name: 'PlaylistTrack'
    fields: =>
      track: @ReferenceField Track, Track.PUBLISH_FIELDS()

  @STATE:
    PENDING: 'pending'
    PLAYING: 'playing'
    PLAYED: 'played'

  @PUBLISH_FIELDS: ->
    track: 1
    order: 1
    playStart: 1
    playEnd: 1
    state: 1

  # If "order" is not specified, track is added at the end of the playlist.
  @addTrack: (track, order) ->
    # We are loading packages in unordered mode, so we are fixing imports here, if needed.
    ScheduleJob = Package.jobs.ScheduleJob unless ScheduleJob

    documentId = @documents.insert
      track: track
      # A slight race-condition, but it is not really critical.
      order: order ? @documents.find().count() + 1
      state: @STATE.PENDING

    # Maybe the just added track has to start playing immediately.
    new ScheduleJob().enqueue()

    documentId

if Meteor.isServer
  PlaylistTrack.Meta.collection._ensureIndex
    state: 1
