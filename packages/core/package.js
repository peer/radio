Package.describe({
  name: 'core',
  version: '0.1.0'
});

Package.onUse(function (api) {
  api.versionsFrom('1.4.1');

  // Core dependencies.
  api.use([
    'coffeescript'
  ]);

  // 3rd party dependencies.
  api.use([
    'peerlibrary:peerdb@0.23.1',
    'peerlibrary:peerdb-migrations@0.3.0',
    'peerlibrary:classy-job@0.6.1'
  ]);

  // Internal dependencies.
  api.use([
    'jobs'
  ], {unordered: true});

  api.export('Album');
  api.export('Artist');
  api.export('Track');
  api.export('JobQueue');
  api.export('Playlist');
  api.export('PlaylistTrack');

  api.addFiles([
    'worker.coffee'
  ], 'server');

  api.addFiles([
    'base.coffee',
    'playlist.coffee',
    'documents/artist.coffee',
    'documents/album.coffee',
    'documents/track.coffee',
    'documents/jobqueue.coffee',
    'documents/playlist-track.coffee',
    'finalize-documents.coffee'
  ]);

  api.addAssets([
    'public/track.svg'
  ], 'client');
});
