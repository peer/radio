Package.describe({
  name: 'jobs',
  version: '0.1.0'
});

Package.onUse(function (api) {
  api.versionsFrom('1.4.1');

  // Core dependencies.
  api.use([
    'coffeescript',
    'http',
    'random',
    'underscore'
  ]);

  // 3rd party dependencies.
  api.use([
    'peerlibrary:assert@0.2.5',
    'peerlibrary:classy-job@0.6.1',
    'peerlibrary:directcollection@0.6.2'
  ]);

  // Internal dependencies.
  api.use([
    'core'
  ]);

  api.export('ScheduleJob');

  api.addFiles([
    'schedule.coffee',
    'shiva.coffee'
  ], 'server');
});
