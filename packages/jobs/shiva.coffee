url = Npm.require 'url'

# TODO: Remove once PeerDB provides findAndModify.
#       See: https://github.com/peerlibrary/meteor-peerdb/issues/37
directArtist = new DirectCollection Artist.Meta.collection._name
directAlbum = new DirectCollection Album.Meta.collection._name
directTrack = new DirectCollection Track.Meta.collection._name

class ShivaSync extends Job
  @register()

  @POLL_INTERVAL: 60 * 1000 # ms

  constructor: (data) ->
    super

    @syncId = Random.id()

    throw new Error "Meteor setting 'shiva.apiEndpoint' missing." unless Meteor.settings?.shiva?.apiEndpoint

    @tracksApiUrl = "#{Meteor.settings.shiva.apiEndpoint}/tracks"

    # Mappings between Shiva API ID and documents with only PUBLISH_FIELDS fields.
    @knownArtists = {}
    @knownAlbums = {}
    @knownTracks = {}

  enqueueOptions: (options) =>
    options = super

    _.defaults options,
      priority: 'medium'
      retry:
        wait: @constructor.POLL_INTERVAL
      repeat:
        wait: @constructor.POLL_INTERVAL
      delay: @constructor.POLL_INTERVAL / 2
      save:
        cancelRepeats: true

  run: ->
    try
      result = HTTP.get @tracksApiUrl,
        auth: Meteor.settings?.shiva?.apiAuth or null
        params:
          fulltree: true
    catch error
      if error.response?.statusCode is 401 and error.response?.data?.message is 'Current user is not logged in'
        throw new Error "Not authenticated against the Shiva API endpoint. Provide Meteor setting 'shiva.apiAuth' as '<username>:<password>' string."

      throw error

    # First we make sure we populate all known artists.
    @ensureArtist track.artist for track in result.data

    # Then we process all tracks.
    for track in result.data
      # TODO: Remove when we will have a way for users to add tracks to the playlist.
      trackAlreadyExists = Track.documents.exists
        shivaId: track.id

      track = @ensureTrack track

      # TODO: Remove when we will have a way for users to add tracks to the playlist.
      # Add track to the end of the playlist.
      PlaylistTrack.addTrack track unless trackAlreadyExists

    # We remove all documents which have old syncId. This works correctly only if there is only one sync run
    # happening at the same time. This should be true because we are using "cancelRepeats" when enqueuing the job.

    Track.documents.update
      syncId:
        $ne: @syncId
    ,
      $set:
        deleted: true
        syncId: null
    ,
      multi: true
    Album.documents.update
      syncId:
        $ne: @syncId
    ,
      $set:
        deleted: true
        syncId: null
    ,
      multi: true
    Artist.documents.update
      syncId:
        $ne: @syncId
    ,
      $set:
        deleted: true
        syncId: null
    ,
      multi: true

    # Length of tracks in playlists potentially just changed. Although, if we are serious about this, we should
    # probably wait for PeerDB to finish updating lengths in PlaylistTrack documents before running scheduling job.
    new ScheduleJob().enqueue()

    return

  absoluteUrl: (relative) ->
    return null unless relative

    url.resolve @tracksApiUrl, relative

  ensureArtist: (artist) ->
    return null unless artist

    unless "#{artist.id}" of @knownArtists
      @knownArtists[artist.id] = directArtist.findAndModify
        shivaId: artist.id
      ,
        []
      ,
        $set:
          shivaId: artist.id
          name: artist.name or ''
          image: @absoluteUrl artist.image
          syncId: @syncId
          deleted: false
        $setOnInsert:
          _id: directArtist._makeNewID()
      ,
        upsert: true
        new: true
        fields: Artist.PUBLISH_FIELDS()

    @knownArtists[artist.id]

  ensureAlbum: (album) ->
    return null unless album

    unless "#{album.id}" of @knownAlbums
      @knownAlbums[album.id] = directAlbum.findAndModify
        shivaId: album.id
      ,
        []
      ,
        $set:
          shivaId: album.id
          name: album.name or ''
          year: album.year or null
          cover: @absoluteUrl album.cover
          artists: (@ensureArtist artist for artist in album.artists or [])
          syncId: @syncId
          deleted: false
        $setOnInsert:
          _id: directAlbum._makeNewID()
      ,
        upsert: true
        new: true
        fields: Album.PUBLISH_FIELDS()

    @knownAlbums[album.id]

  ensureTrack: (track) ->
    return null unless track

    unless "#{track.id}" of @knownTracks
      @knownTracks[track.id] = directTrack.findAndModify
        shivaId: track.id
      ,
        []
      ,
        $set:
          shivaId: track.id
          bitrate: track.bitrate ? null
          album: @ensureAlbum track.album
          artist: @ensureArtist track.artist
          length: track.length ? null
          # ownCloud Music app Shiva API uses "number" instead of "ordinal".
          ordinal: track.ordinal ? track.number ? null
          title: track.title or ''
          files: _.object ([mimeType, @absoluteUrl relative] for mimeType, relative of track.files or {})
          syncId: @syncId
          deleted: false
        $setOnInsert:
          _id: directTrack._makeNewID()
      ,
        upsert: true
        new: true
        fields: Track.PUBLISH_FIELDS()

    @knownTracks[track.id]

if Meteor.settings?.shiva
  Meteor.startup ->
    # Start a periodic job. "enqueueOptions" configures "cancelRepeats" which
    # cancels any previous job of the same type from the queue.
    new ShivaSync().enqueue()
