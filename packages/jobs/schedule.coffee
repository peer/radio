# For this job we want that it runs serially without any parallelism, but we do want
# that the job is enqueued even if it is already running so that we recompute the
# schedule as soon as possible for the playlist change which triggered enqueuing.
# So "skipIfExisting" does not help us here so we have to do it ourselves.

# TODO: Remove once PeerDB provides findAndModify.
#       See: https://github.com/peerlibrary/meteor-peerdb/issues/37
directPlaylistTrack = new DirectCollection PlaylistTrack.Meta.collection._name

class ScheduleJob extends Job
  @register()

  run: ->
    currentlyPlaying = PlaylistTrack.documents.findOne
      state: PlaylistTrack.STATE.PLAYING

    if currentlyPlaying
      currentTime = new Date()

      assert currentlyPlaying.track.length >= 0, currentlyPlaying.track.length

      if currentlyPlaying.playStart.valueOf() + currentlyPlaying.track.length * 1000 <= currentTime.valueOf()
        PlaylistTrack.documents.update currentlyPlaying._id,
          $set:
            playEnd: currentTime
            state: PlaylistTrack.STATE.PLAYED
            order: null

        # TODO: Remove when we will have a way for users to add tracks to the playlist.
        # Add track to the end of the playlist.
        PlaylistTrack.addTrack currentlyPlaying.track

        # No track is currently playing (anymore).
        currentlyPlaying = null
      else
        # Just to be sure, if maybe the track length got updated.
        currentlyPlaying.playEnd = new Date currentlyPlaying.playStart.valueOf() + currentlyPlaying.track.length * 1000
        changed = PlaylistTrack.documents.update
          _id: currentlyPlaying._id
          playEnd:
            $ne: currentlyPlaying.playEnd
        ,
          $set:
            playEnd: currentlyPlaying.playEnd

        # If end time changed we want to schedule an extra job to handle moving to the next track at the right time.
        @nextScheduleJob currentlyPlaying.playEnd if changed

    # We check "currentlyPlaying" again because we maybe set it to null above.
    unless currentlyPlaying
      currentTime = new Date()

      # Let's start playing the next song.
      currentlyPlaying = directPlaylistTrack.findAndModify
        state: PlaylistTrack.STATE.PENDING
      ,
        [['order', 1]]
      ,
        $set:
          order: null
          state: PlaylistTrack.STATE.PLAYING
          playStart: currentTime
          # We would need to know the length of the found track to know to what should we set this.
          # So let us set it to null for now and make another query to fix it.
          playEnd: null
      ,
        new: true

      if currentlyPlaying
        assert currentlyPlaying.track.length >= 0, currentlyPlaying.track.length

        # We have to make another query to set the "playEnd".
        playEnd = new Date currentTime.valueOf() + currentlyPlaying.track.length * 1000
        currentlyPlaying = directPlaylistTrack.findAndModify
          _id: currentlyPlaying._id
        ,
          []
        ,
          $set:
            playEnd: playEnd
        ,
          new: true

        assert currentlyPlaying

        # TODO: Remove once PeerDB provides findAndModify.
        #       See: https://github.com/peerlibrary/meteor-peerdb/issues/37
        currentlyPlaying = PlaylistTrack.Meta.collection._transform currentlyPlaying if PlaylistTrack.Meta.collection._transform

        @nextScheduleJob currentlyPlaying.playEnd

    # If "currentlyPlaying" is not set at this stage it means that there are no pending tracks.
    return unless currentlyPlaying

    lastPlayEnd = currentlyPlaying.playEnd
    PlaylistTrack.documents.find(
      state: PlaylistTrack.STATE.PENDING
    ,
      sort:
        order: 1
    ).forEach (track, i, cursor) =>
      assert track.track.length >= 0, track.track.length

      newPlayEnd = new Date lastPlayEnd.valueOf() + track.track.length * 1000
      PlaylistTrack.documents.update track._id,
        $set:
          playStart: lastPlayEnd
          playEnd: newPlayEnd

      lastPlayEnd = newPlayEnd

    return

  shouldSkip: (options) ->
    return true if super

    # If there is already a job waiting or ready, but not running, we do not enqueue
    # a new job because the existing job will do the necessary work for us later,
    # using the state available then which will include also changes which triggered
    # the new job to be enqueued. So we do not have to enqueue the new job.
    !!JobsWorker.collection.findOne
      type: @type()
      status:
        $in: ['waiting', 'ready']
    ,
      fields:
        _id: 1
      transform: null

  enqueueOptions: (options) ->
    options = super

    options = _.defaults options,
      priority: 'high'

    # If there is existing job which has not yet completed, make it a dependency
    # of this job. This effectively puts all jobs into a FIFO queue.
    jobQueueItem = JobsWorker.collection.findOne
      type: @type()
      status:
        $in: JobsWorker.collection.jobStatusCancellable
    ,
      sort:
        created: -1
    if jobQueueItem
      options.depends ?= []
      options.depends.push JobsWorker._makeJob jobQueueItem

    options

  nextScheduleJob: (playEnd) ->
    new ScheduleJob().enqueue
      after: playEnd

    # Because jobs are promoted (detected that "after" has been reached) only at the JobsWorker.PROMOTE_INTERVAL
    # interval, we trigger check for the promotion ourselves at the precise time.
    Meteor.setTimeout =>
      JobsWorker.promote()
    ,
      playEnd.valueOf() - new Date().valueOf()
